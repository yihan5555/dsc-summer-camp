#!/bin/sh

sudo apt update
sudo apt install -y git apache2 php libapache2-mod-php
sudo cp index.php /var/www/html/
sudo rm /var/www/html/index.html
sudo service apache2 restart
